export interface IParsedPath {
  path: string;
  name: string;
  isFile: boolean;
}

export type ConverterOptions = {
  quality: number;
  clear: boolean;
};
