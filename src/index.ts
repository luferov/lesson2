import {ConverterOptions} from './types';
import {convert} from './converter';
import {Command, Option} from 'commander';

const program = new Command();

program
  .name('convert')
  .description('CLI to convert pong, jpg files to webp format.')
  .version('0.0.1');

program
  .command('transform')
  .alias('t')
  .description('Transform png, jpg images to webp format.')
  .argument('<string>', 'Input folder or file')
  .argument('<string>', 'Output folder or file')
  .addOption(new Option('-q, --quality', 'Quality of image').default(100))
  .addOption(
    new Option('-c, --clear', 'Clear output folder if exists')
      .default('false')
      .choices(['true', 'false'])
  )
  .action(async (ip: string, op: string, options: Record<string, unknown>) => {
    const converterOptions: ConverterOptions = {
      quality: Number(options.quality),
      clear: options.clear === 'true',
    };
    await convert(ip, op, converterOptions);
  });

program.parse();
