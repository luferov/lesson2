import process from 'process';
import path from 'node:path';
import {existsSync} from 'fs';
import fs from 'fs/promises';

import rimraf from 'rimraf';
import cliProgress from 'cli-progress';
import webp from 'webp-converter';
import {IParsedPath, ConverterOptions} from './types';

const defaultOptions: ConverterOptions = {
  quality: 100,
  clear: false,
};
/**
 * Function of convert jpg, png to webp
 * @param ip Input path
 * @param op Output path
 * @param converterOptions Options
 */
async function convert(
  ip: string,
  op: string,
  converterOptions: ConverterOptions
): Promise<string | string[]> {
  const options = {...defaultOptions, ...converterOptions};
  const inputFile = await makeAbsolutePath(ip);
  const outputFile = await makeAbsolutePath(op);
  if (inputFile.isFile) {
    const outputPattern: string = outputFile.isFile
      ? outputFile.path
      : `${outputFile.path}/${inputFile.name}.webp`;
    await webp.cwebp(inputFile.path, outputPattern, `-q ${options.quality}`);
    return outputPattern;
  }
  options.clear && (await rimraf(outputFile.path));

  const inputPattern: string = inputFile.path;
  const outputPattern: string = outputFile.isFile
    ? path.dirname(outputFile.path)
    : outputFile.path;
  options.clear && (await rimraf(outputPattern));
  if (!existsSync(outputPattern)) {
    await fs.mkdir(outputPattern);
  }
  const progressBar = new cliProgress.SingleBar(
    {},
    cliProgress.Presets.shades_classic
  );
  const files = await fs.readdir(inputPattern);
  progressBar.start(files.length, 0);
  const readyFiles = [];
  for (const file of files) {
    const extFile = path.extname(file);
    if (['.jpg', '.png'].includes(extFile)) {
      const fileName = path.basename(file, path.extname(file));
      const fp = path.join(inputPattern, file);
      const fo = path.join(outputPattern, `${fileName}.webp`);
      await webp.cwebp(fp, fo, `-q ${options.quality}`);
      readyFiles.push(fp);
    }
    progressBar.increment();
  }
  progressBar.stop();
  return readyFiles;
}

/**
 * Make absolute path
 * @param filePath file or folder path
 * @returns absolute path
 */
async function makeAbsolutePath(filePath: string): Promise<IParsedPath> {
  const parsedPath = path.parse(filePath);
  return {
    path: path.format({
      ...parsedPath,
      root: parsedPath.root || process.cwd() + '/',
    }),
    name: parsedPath.name,
    isFile: parsedPath.ext !== '',
  };
}

export {convert};
